import pandas as pd
from sklearn import linear_model 
import matplotlib.pyplot as plt
from sklearn.metrics import mean_absolute_error

#Carga de archivo 
datos = pd.read_csv("grados.csv");

#Extracción de datos del archivo
nombrePrimerColumna=datos.columns[0];
nombreSegundaColumna=datos.columns[0];

datosPrimerColumna=datos[[datos.columns[0]]]
datosSegundaColumna=datos[[datos.columns[1]]]

#Graficar los nombres de las variables
plt.xlabel(nombrePrimerColumna);
plt.ylabel(nombreSegundaColumna);


#Instanciar el objeto de regresion lineal
reg = linear_model.LinearRegression();

#Entrenamiento
reg.fit(datos[[nombrePrimerColumna]], datosSegundaColumna)

#celcius a predecir
celcius_predecir = [21,22,80]

#datos para evaluacion
fahrenheit_reales = [[69.8, 71.6, 176]]

#prediccion
prediccionA = round(reg.predict([[celcius_predecir[0]]])[0][0],1)
prediccionB = round(reg.predict([[celcius_predecir[1]]])[0][0],1)
prediccionC = round(reg.predict([[celcius_predecir[2]]])[0][0],1)

#prediccion hecha por algoritmo de regresion lineal simple
print("Celcius - Fahrenheit \n", celcius_predecir[0], prediccionA,"\n", celcius_predecir[1], prediccionB, "\n", celcius_predecir[2], prediccionC)
fahrenheit_predichos = [prediccionA, prediccionB, prediccionC]

#margen de error
mse = mean_absolute_error(fahrenheit_reales, [fahrenheit_predichos])

print("\nMargen de error: (cuando mas cerca del 0 este mejor) = ",mse)

#Graficar los datos y obtener la funcion lineal
plt.plot(datosPrimerColumna, reg.predict(datos[[nombrePrimerColumna]]),color='blue')
plt.show()

print("Coeficiente: ", reg.coef_) #m
print("Intercepta en: ",reg.intercept_) #b
print("formula lineal encontrada = ", "celcius * ", reg.coef_ ,  " + " , reg.intercept_)

